/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convertpdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

/**
 *
 * @author Sammy Guergachi <sguergachi at gmail.com>
 */
public class ConvertPdf {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        getPDFlines(args[0]);
        getPDFlines("03TRT_03102016.pdf");
    }
    
    public static void getPDFlines(String pathPdf) {
        try {
            File arquivo_pdf = new File(pathPdf);

            if (arquivo_pdf.exists()) {
                String ans[] = null;
                PDDocument doc = PDDocument.load(arquivo_pdf);
                PDFTextStripper stripper = new PDFTextStripper();
                stripper.setStartPage(1);
                stripper.setEndPage(Integer.MAX_VALUE);
                System.out.println("Iniciando conversão do arquivo: " + pathPdf);
                ans = stripper.getText(doc).split("\r\n");
                doc.close();
                File arquivoTxt = new File(pathPdf.replace(".pdf", ".txt"));
                OutputStreamWriter bufferOut = new OutputStreamWriter(new FileOutputStream(arquivoTxt.getAbsolutePath()), StandardCharsets.UTF_8);
                System.out.println("Conversão finalizada. Salvando TXT...");
                int contador = 0;
                for(String linha : ans){
                    bufferOut.write(linha+"\r\n");
                    contador++;
                    System.out.print(contador+"\r");
                }
                bufferOut.close();
                System.out.println("\nOperação finalizada.");
            } else {
               System.out.println("Arquivo para conversão inválido.");
               System.out.println(pathPdf);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
